#########  Purpose of this document  ###########################################

This document explain the process of configuring how 
JWM Kit Logout and JWM Kit Time request authentication.
The Graphical configuration covered below is recommended


############ IMPORTANT NOTICE ##################################################

- JWMKit Logout and Time no longer supports commandline parameter
- Commandline parameters only exist for backward compatibility
- Commandline Parameters are no longer documented.
- jwmkit_logout will disable sleep when started with a commandline parameter

If you wish to manually configuring these settings then do so by editing the
file $HOME/.config/jwmkit/setting_su


######### * Graphical Configuration * ##########################################

Just type the following into a your terminal:
jwmkit_first_run

Then click the "Configure" button and select your options from the GUI.
Remember to press the 'save' button when done.


######### Keyword used in the file ~/.config/jwmkit/setting_su #################

Keywords used by the setting_su file:

logout -------- How JWMKit performs poweroff/reboot.
time ---------- How JWMKit sets the time.
editor -------- Configure a text editor for JWMKit instead of the built-in viewer
archiver ------ Configure an archiver for JWMKit instead of the built-in viewer
filemanager --- Configure a filemanager for JWMKit (rarly used)
suspend ------- command or tool used for suspend
hibernate ----- command or tool used for hibernate
sleep --------- authentication or tool used to hibernate and suspend.
term ---------- preferred terminal emulator

use an equal sign (=) to assign a value to a keyword. Example:

    term=xfce4-terminal


######### The Logout Keyword Values ############################################

The logout keyword defines how JWMKit preforms reboot/shutdown. Options include
services (elogind, sysd, etc) and authentication (sudo, su, etc)


-- Values for services/tools. -----------------------------------------------

empty (no value) - Auto configure (more info below)
consolekit ------- Use dbus and consolekit
elogind ---------- use elogind's loginctl command
puppy ------------ use Puppy Linux's wmreboot and wmpoweroff command


-- Values for requesting permission ----------------------------------------
-- NOTE: Values are used with the commands /sbin/reboot and /sbin/poweroff -

su --------------- su via terminal
sudop ------------ built in prompt
nosu ------------- Do not ask permssion 
gksu_r ----------- gksu 
gksu ------------- fake gksu 
gksudo_r --------- gksudo
gksudo ----------- fake gksudo
nox -------------- su-to-root -c
x ---------------- su-to-root -X -c
sudo ------------- sudo without a password
sudoa ------------ sudo -A ( sudo --askpass )
sysd ------------- systemd's systemctl
doas ------------- doas without a password
tdoas ------------ doas via terminal

Simple Example ----------

    logout=sudop

-- Advance Values (for custom commands) ------------------------------------

custom ----------- reboot and shutdown command separated with a semicolon 
term ------------- custom authentication via terminal 

NOTE : term uses the commands /sbin/reboot and /sbin/poweroff

-- Advance values Syntax ---------------------------------------------------

custom:[reboot command]:[shutdown command]
term:[chosen authentication tool and it's parameters]

--- Examples of the custom option ------------------------------------------

This example will use the my_reboot command to reboot, 
and the my_off for power off

    logout=custom:my_reboot:my_off

Same example using sudo to request permission

    logout=custom:sudo my_reboot:sudo my_off

This last example launches a terminal (for user input)
logout=custom:xterm -e "sudo my_reboot":xterm -e "sudo my_off"

--- Examples of the term option --------------------------------------------

use sudo with the default /sbin/reboot and /sbin/poweroff via the terminal

    term:sudo


######### The Sleep Keyword Values #############################################

The sleep keyword defines how JWMKit preforms suspend/hibernate. Options include
services (elogind, sysd, etc) and authentication (sudo, su, etc)

Sleep supports the following unique value

upower ----------- dbus/upower

The sleep keyword shares the follow values with the logout keyword:

    empty (no value), su, sudo, nosu, sudop, gksu, gksu_r, gksudo, gksudo_r,
    x, nox, doas, tdoas, term, sysd, elogind

All of values work the same as they do with the logout keyword. Reference the
information provided for the logout keyword for details.

Simple Example ----------

    sleep=doas


######### The Suspend Keyword Values ###########################################

The suspend Keyword defines command JWMKit uses to suspend the machine.
The command is uses in conjunction with the values set by the sleep keyword.
For example.

Suspend supports the following unique value

    empty (no value) - disable suspend (or exclude suspend= from the file)
    Suspend ---------- use this value when sleep is set to: elogind, sysd, upower
    echo: ------------ value to write to /sys/power/state

It is also possible to set the suspend value to any command that will
suspend your PC. Common commands are:

    pm-suspend
    pmi action suspend


Examples of suspend=echo:

    suspend=echo:mem       is the same as    echo -n mem > /sys/power/state
    suspend=echo:freeze    is the same as    echo -n freeze > /sys/power/state
    suspend=echo:disk      is the same as    echo -n disk > /sys/power/state

Simple Examples ----------

    suspend=Suspend
    suspend=pm-suspend

NOTE : The section "Relationship between sleep and suspend/hibernate keyword" at
the end of this document for an explanation of how these parameters work together


######### The Hibernate Keyword Values #########################################

The hibernate Keyword defines command JWMKit uses to hibernate the machine.
The command is uses in conjunction with the values set by the sleep keyword.

To disable hibernate either excluding the hibernate= value from the setting_su
file or set the value to nothing like this: hibernate=

    Hibernate --- use this value when sleep is set to: elogind, sysd, upower
    echo: ------- value to write to /sys/power/state

Example -----------------

    hibernate=Hibernate


Otherwise set the hibernate value to any command that will hibernate your PC.

Common commands are:

    pm-hibernate
    pmi action hibernate


Simple Example ----------

    hibernate=pm-hibernate

NOTE : The section "Relationship between sleep and suspend/hibernate keyword" at
the end of this document for an explanation of how these parameters work together

Relationship between sleep and suspend/hibernate keyword


######### The Time Keyword Values ##############################################

The time keyword defines how JWMKit modifies system time. Options include
systemd and authentication (sudo, su, etc)

The Time keyword shares the follow values with the logout keyword:

    empty (no value), susudop, nosu, gksu_r, gksu,  gksudo_r, gksudo,
    nox, x, sudo, sudoa, sysd, term, doas, tdoas

All of values work the same as they do with the logout keyword. Reference the
information provided for the logout keyword for details.

NOTE: the sysd setting will use systemd's timedatectl

Simple Example ----------

    time=sudop


######### Keywords - Not Required  #############################################

JWMKit does not require the following keywords to be configured. They only
exsit to allow users to further customize their experience.

    editor, archiver, filemanager

To configure these keywords simply set the value to the command that runs
the preferred tool. 

Example -----------------

    editor=mousepad


######### Relationship between sleep and suspend/hibernate keyword #############

As explained previously, the sleep keyword defines a services or authentication
to use with the values defined with sleep/hibernate keyword


-- sleep as a service --
If a service is defined then the appropriate command for that service is issued.
In this case the sleep/hibernate keyword defines which command to use.
Typically you set suspend=Suspend, and hibernate=Hibernate, but you could
replace with other supported options supported by the service like hybrid-sleep.
This also means you could swap function of a button For example by adding
hibernate=Suspend to the setting_su would make the hibernate button suspend the
PC instead of hibernate.

This means the when sleep=upower a long dbus-send command is used, but when the
values are elgoind or sysd is much simpler and the command is the sleep value
preceding the suspend or hibernate value.


-- sleep for authentication --
If sleep defines a means of authentication then typically the authentication
command will preceded the suspend/hibernate command.

Example of sleep for authentication with the following values:

    sleep=sudo
    suspend=pm-suspend

Then the complete command used will be

    sudo pm-suspend


################################ END ###########################################