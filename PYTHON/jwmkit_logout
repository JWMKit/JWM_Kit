#!/usr/bin/python3
import gi, os, sys, jwmkit_utils
from subprocess import run, PIPE
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf

# JWM Kit - A set of Graphical Apps to simplify use of JWM (Joe's Window Manager) <https://codeberg.org/JWMKit/JWM_Kit>
# Copyright © 2020-2024 Calvin Kent McNabb <apps.jwmkit@gmail.com>
#
# This file is part of JWM Kit.
#
# JWM Kit is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License, version 2,
# as published by the Free Software Foundation.
#
# JWM Kit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with JWM Kit.  If not, see <https://www.gnu.org/licenses/>.


def prompt(command):
    pw_data = jwmkit_utils.private_entry(win)
    if not pw_data[0]:
        return
    else:
        if command.startswith(su):
            cmd = command
        else:
            cmd = 'echo "{}" | {} {} && echo success'.format(pw_data[1], 'sudo -S', command)
        auth_check(cmd)
        return cmd


def run_cmd(cmd):
    cmd = 'echo -n {} > /sys/power/state'.format(cmd)
    os.system(cmd)
    os.sys.exit()


def auth_check(cmd):
    check = run(cmd, shell=True, stdout=PIPE)
    check = check.stdout.decode()
    if "success" in check:
        print("OK")
    else:
        print("Fail")
        prompt(cmd)
        return


def hs_command(widget, hs_cmd):
    if sleep == 'upower':
        cmd = 'dbus-send --system --print-reply --dest="org.freedesktop.UPower" /org/freedesktop/UPower ' \
              'org.freedesktop.UPower.{}'.format(hs_cmd)
    elif sleep in ('sysd', 'systemd', 'systemctl'):
        cmd = 'systemctl {}'.format(hs_cmd.lower())
    elif sleep in ('loginctl', 'elogind'):
        cmd = 'loginctl {}'.format(hs_cmd.lower())
    elif sleep == 'nox':
        cmd = '{} -e "su-to-root -c {}"'.format(term, hs_cmd)
    elif sleep == 'x':
        cmd = 'su-to-root -X -c {}'.format(hs_cmd)
    elif sleep == 'su':
        cmd = '{} -e "su -c {}"'.format(term, hs_cmd)
    elif sleep == 'custom':
        cmd = hs_cmd
    elif sleep in ('sudoa', 'askpass'):
        cmd = 'sudo -A {}'.format(hs_cmd)
    elif sleep == 'tdoas':
        cmd = '{} -e "doas {}"'.format(term, hs_cmd)
    elif sleep.startswith('term:'):
        cmd = sleep[5:]
        cmd = '{} -e "{} {}"'.format(term, cmd, hs_cmd)
    elif sleep == 'sudop':
        if hs_cmd.startswith('echo:'):
            prompt('jwmkit_logout {}'.format(hs_cmd))
        prompt(hs_cmd)
        return
    elif sleep in ('sudo', 'su-to-root -X -c', 'gksu', 'gksudo', 'doas', 'pkexec'):
        cmd = "{} {}".format(sleep, hs_cmd)
    else:
        cmd = hs_cmd
    if hs_cmd.startswith('echo:') and sleep.startswith('term:'):
        cmd = cmd.replace(hs_cmd, "jwmkit_logout {}".format(hs_cmd))
    elif hs_cmd.startswith('echo:') and sleep not in ('', 'nosu', 'sudo', 'sudoa', 'tdoas', 'doas'):
        cmd = cmd.replace(hs_cmd, "'jwmkit_logout {}'".format(hs_cmd))
    elif hs_cmd.startswith('echo:') and sleep in ('nosu', 'sudo', 'sudoa', 'tdoas', 'doas'):
        cmd = cmd.replace(hs_cmd, "jwmkit_logout {}".format(hs_cmd))
    else:
        cmd = 'echo -n {} > /sys/power/state'.format(cmd)
    if cmd:
        os.system(cmd)


def reboot_command(widget):
    if sys.argv[1] in ('dbus', 'consolekit'):
        cmd = 'dbus-send --system --print-reply --dest="org.freedesktop.ConsoleKit"' \
              ' /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Restart'
    elif sys.argv[1] == 'x':
        cmd = 'su-to-root -X -c /sbin/reboot'
    elif sys.argv[1] == 'nox':
        cmd = '{} -e "su-to-root -c /sbin/reboot"'.format(term)
    elif sys.argv[1] == 'su':
        cmd = '{} -e "su -c /sbin/reboot"'.format(term)
    elif sys.argv[1] == 'tdoas':
        cmd = '{} -e "doas /sbin/reboot"'.format(term)
    elif sys.argv[1].startswith('custom:'):
        cmd = sys.argv[1].split(':')[1]
    elif sys.argv[1] in ('sudoa', 'askpass'):
        cmd = 'sudo -A /sbin/reboot'
    elif sys.argv[1].startswith("term:"):
        cmd = sys.argv[1].split(":")
        cmd = '{} -e "{} /sbin/reboot"'.format(term, cmd[1])
    elif sys.argv[1] in ('sysd', 'systemd', 'systemctl'):
        cmd = 'systemctl reboot'
    elif sys.argv[1] in ('loginctl', 'elogind'):
        cmd = 'loginctl reboot'
    elif sys.argv[1] == 'sudop':
        prompt("/sbin/reboot")
        return
    elif sys.argv[1] in ('sudo', 'su-to-root -X -c', 'gksu', 'gksudo', 'doas'):
        cmd = sys.argv[1] + " /sbin/reboot"
    elif sys.argv[1] == 'puppy':
        cmd = '/usr/bin/wmreboot'
    elif sys.argv[1] == 'pkexec':
        cmd = 'pkexec /sbin/reboot'
    else:
        cmd = "/sbin/reboot"
    if cmd:
        os.system(cmd)


def shutdown_command(widget):
    if sys.argv[1] in ('dbus', 'consolekit'):
        cmd = 'dbus-send --system --print-reply --dest="org.freedesktop.ConsoleKit"' \
              ' /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Stop'
    elif sys.argv[1] == 'x':
        cmd = 'su-to-root -X -c /sbin/poweroff'
    elif sys.argv[1] == 'nox':
        cmd = '{} -e "su-to-root -c /sbin/poweroff"'.format(term)
    elif sys.argv[1] == 'su':
        cmd = '{} -e "su -c /sbin/poweroff"'.format(term)
    elif sys.argv[1] == 'tdoas':
        cmd = '{} -e "doas /sbin/poweroff"'.format(term)
    elif sys.argv[1].startswith('custom:'):
        cmd = sys.argv[1].split(':')[2]
    elif sys.argv[1] in ('sudoa', 'askpass'):
        cmd = 'sudo -A /sbin/poweroff'
    elif sys.argv[1].startswith("term"):
        cmd = sys.argv[1].split(":")
        cmd = '{} -e "{} /sbin/poweroff"'.format(term, cmd[1])
    elif sys.argv[1] in ('sysd', 'systemd', 'systemctl'):
        cmd = 'systemctl poweroff'
    elif sys.argv[1] in ('loginctl', 'elogind'):
        cmd = 'loginctl poweroff'
    elif sys.argv[1] == 'sudop':
        prompt("/sbin/poweroff")
        return
    elif sys.argv[1] in ('sudo', 'su-to-root -X -c', 'gksu', 'gksudo', 'doas'):
        cmd = sys.argv[1] + " /sbin/poweroff"
    elif sys.argv[1] in ('puppy', 'wm'):
        cmd = '/usr/bin/wmpoweroff'
    elif sys.argv[1] == 'pkexec':
        cmd = 'pkexec /sbin/poweroff'
    else:
        cmd = "/sbin/poweroff"
    if cmd:
        os.system(cmd)


def make_image(icon):
    button_image = Gtk.Image()
    pb = GdkPixbuf.Pixbuf.new_from_file_at_scale(icon, 50, 50,
                                                 preserve_aspect_ratio=True)
    button_image.set_from_pixbuf(pb)
    return button_image


def config_buttons(button):
    button.set_property("width-request", 120)
    button.set_always_show_image(True)
    button.set_image_position(Gtk.PositionType.TOP)


class GridWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="Logout  {}".format(os.environ["USER"]))
        try:
            self.set_icon_from_file(jwmkit_utils.find_icon('exitgray.svg'))
        except gi.repository.GLib.Error:
            self.set_icon_name("application-exit")
        self.set_border_width(6)
        self.set_hexpand(False)
        grid = Gtk.Grid(column_homogeneous=True, column_spacing=10, row_spacing=10)
        self.add(grid)

        self.cancel_button = Gtk.Button(image=make_image(jwmkit_utils.find_icon('cancelgray.svg')), label="Cancel")
        self.logout_button = Gtk.Button(image=make_image(jwmkit_utils.find_icon('exitgray.svg')), label="Logout")
        self.reboot_button = Gtk.Button(image=make_image(jwmkit_utils.find_icon('restartgray.svg')), label="Reboot")
        self.shutdown_button = Gtk.Button(image=make_image(jwmkit_utils.find_icon('shutdowngray.svg')), label="Shutdown")
        self.suspend_button = Gtk.Button(image=make_image(jwmkit_utils.find_icon('suspendgray.svg')), label="Suspend")
        self.hibernate_button = Gtk.Button(image=make_image(jwmkit_utils.find_icon('hibernategray.svg')), label="Hibernate")

        config_buttons(self.cancel_button)
        config_buttons(self.logout_button)
        config_buttons(self.reboot_button)
        config_buttons(self.shutdown_button)
        config_buttons(self.suspend_button)
        config_buttons(self.hibernate_button)

        # place buttons on grid
        grid.attach(self.cancel_button, 0, 0, 6, 11)
        grid.attach(self.reboot_button, 11, 0, 6, 11)
        grid.attach(self.logout_button, 0, 11, 6, 11)
        grid.attach(self.shutdown_button, 11, 11, 6, 11)
        if s_cmd:
            grid.attach(self.suspend_button, 22, 0, 6, 11)
        if h_cmd:
            grid.attach(self.hibernate_button, 22, 11, 6, 11)

        # link buttons to actions
        self.cancel_button.connect("clicked", Gtk.main_quit)
        self.logout_button.connect("clicked", self.logout_command)
        self.reboot_button.connect("clicked", reboot_command)
        self.shutdown_button.connect("clicked", shutdown_command)
        self.suspend_button.connect("clicked", hs_command, s_cmd)
        self.hibernate_button.connect("clicked", hs_command, h_cmd)

    def disable_buttons(self):
        self.cancel_button.set_sensitive(False)
        self.logout_button.set_sensitive(False)
        self.reboot_button.set_sensitive(False)
        self.shutdown_button.set_sensitive(False)
        self.suspend_button.set_sensitive(False)
        self.hibernate_button.set_sensitive(False)

    # button actions
    def logout_command(self, widget):
        self.disable_buttons()
        if len(sys.argv) < 2:
            os.system("jwm -exit")
        elif sys.argv[1] in ['puppy', '-puppy']:
            os.system("/usr/bin/wmexit")
        else:
            os.system("jwm -exit")


# if "echo:" in parm pass the command to the system and exit
if len(sys.argv) >= 2:
    if sys.argv[1][:5] == 'echo:':
        run_cmd(sys.argv[1][5:])

pid = str(os.getpid())
pids = os.popen('pgrep -x "jwmkit_logout"').read()
pids = str(pids).split('\n')
if pid in pids:
    pids.remove(pid)
pids = [id for id in pids if id]
if pids:
    os.system("kill -s TERM " + pids[0])
    os.sys.exit()

if len(sys.argv) >= 2:
    # Remove leading dashes ( - ) from input parameters
    while sys.argv[1][0] == "-": sys.argv[1] = sys.argv[1][1:]
    sys.argv[1] = sys.argv[1].lower().replace('root -x -c', 'root -X -c')
    s_cmd, h_cmd = '', ''
else:
    # check parameters are defined in configuration file
    su = jwmkit_utils.get_su('logout')
    s_cmd = jwmkit_utils.get_su('suspend')
    h_cmd = jwmkit_utils.get_su('hibernate')
    term = jwmkit_utils.get_su('term')
    sleep = jwmkit_utils.get_su('sleep')

    if not term:
        term = jwmkit_utils.find_terminal()
        jwmkit_utils.edit_ssu('term', term)
    if sleep:
        if sleep.endswith('_r'):
            sleep = sleep[:-2]
    if su:
        if su.endswith('_r'):
            su = su[:-2]
        sys.argv.append(su)
    else:
        su = jwmkit_utils.login_auto_config()
        if not su:
            jwmkit_utils.app_fail()
        else:
            sys.argv.append(su)
            jwmkit_utils.edit_ssu('logout', su)

win = GridWindow()
win.connect("delete-event", Gtk.main_quit)
win.set_resizable(False)
win.set_position(Gtk.WindowPosition.CENTER)
win.show_all()
Gtk.main()
