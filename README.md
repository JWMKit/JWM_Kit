﻿| [Updates](#updates) | 
[Dependencies](#dependencies) | 
[Downloads](#downloads) | 
[Features](#some-of-jwm-kits-features) | 
[Tools](#summary-of-jwm-kits-tools) | 
[Behavior](#behavior-that-may-or-may-not-be-obvious) |
[Validation](#validation-check) |
[Wiki](https://codeberg.org/JWMKit/JWM_Kit/wiki) |
 [SoundBraid](https://codeberg.org/JWMKit/soundbraid)  |

## JWM Kit

**A set of software designed to simplify usage of JWM (Joe's Window Manager)**  

JWM Kit is Free Open Source Software.  
See [License](https://codeberg.org/JWMKit/JWM_Kit/src/branch/master/doc/LICENSE). 
 
 ***

### UPDATES

**August 25 2024**  

JWMKit now works with FreeBSD - Although no new package or release has been made.

The following changes should imporve portablity with systems that package software in /usr/local/  

- JWMKit Icons can now be placed in either  
 /usr/share/pixmaps/jwmkit  
 /usr/local/share/pixmaps/jwmkit
 - JWMKit documents can be placed in either  
  /usr/share/doc/jwmkit/  
/usr/share/doc/packages/jwmkit/   
/usr/local/share/doc/jwmkit/  
 - Processing of .desktop files now includes /usr/local/share/applications
- Searching of icons now includes /usr/local/share/icons/ and /usr/local/share/pixmaps/

Other Changes:

- Optimized and expanded searching for icons
- Expanded searching for .desktop files.

**August 15 2024**  

Packages are now avaliable for Fedora, and openSUSE on the JWMKit  [Sourceforge page](https://sourceforge.net/projects/jwmkit/files/Packages/) .  
The files used to build the rpms along with some instructions will be added soon.

**March 28, 2024**

New Stable Release

You can find all change listed in this update log from Feb 8 to March 27.

Not to repeat myself but It is necessary to ensure users are aware of the following notice.

-- Important Notice --   

Consolekit is no longer the default for jwmkit_logout. It must be configured.
If logout quits working (or changes it's behavior) after updating to the newest version most likely you need to run jwmkit_first_run and select the consolekit option for logout.

DOWNLOADS : New packages and build scripts are avaliable at the [JWMKit Linux sourceforge page](https://sourceforge.net/projects/jwmkit/files/) 


**March 27, 2024**

Big update for logout, time and first_run.  First off a warning.

-- Important Notice --   

Consolekit is no longer the default for jwmkit_logout. It must be configured.
If logout quits working (or changes it's behavior) after updating to the newest version most likely you need to run jwmkit_first_run and select the consolekit option for logout.

-- Changes ----------------

- Auto configure. jwmkit_time & jwmkit_logout will attempt to configure themselves if not already configured. The configuration will be saved.
- FYI : Auto configure chooses compatibility over eloquence.   "Nicer" solutions maybe available by configured with  jwmkit_first_run.
- jwmkit_first_run : Hide option not installed on the system. Cleaner and less confusing for the user. 
- jwmkit_first_run : added a revert button for the terminal entry.
- jwmkit_logout  will launch jwmkt_first_run if it is necessary for the user to configure it.
- jwmkit_logout's suspend/hibernation functionality is now fully implemented and easier to use. 
- jwmkit_logout will disable suspend/hibernation when started with a commandline parameter. New parameters will not be added, but this will allows the old ones to continue workings.
- jwmkit_logout : Sleep commands using > to redirect to a file are launched as a separate instance  to guarantee authentication.  
- [setting_su synatx](https://codeberg.org/JWMKit/JWM_Kit/src/branch/master/doc/README_logout.txt) has changes, but compatible is maintained. 

**March 1, 2024**

- Bugfix -  syntax error in .jwmrc could prevent jwmkit_repair from starting.
- Added info to error_help.txt explaining how improper file permission could trigger the *possible executable without leading "exec:"*  warning. 
- Copyright date update to 2024

**Feb 8, 2024**

In order to improve compatibility with various terminal emulators, JWMKit will no longer set the window title for terminal applications. 

**Dec 23, 2023**  

- Improve collection of time zone data

**Nov 27, 2023**  

- jwmkit_keys now supports specifying a window corner when configuring a key/mouse binding for resizing the window.
- Fixed a bug introduced in recent commit where  jwmkit_keys would fail to open the edit window in certain conditions.

**Nov 13, 2023**   - Second Entry

*BUGFIX* - jwmkit_logout's new suspend and hibernation actions were disable for testing.  Sorry if anyone tried to use the previous version and it failed. It should be fixed.
  
**Nov 13, 2023**

jwmkit\_first\_run has been updated to support the configuration of jwmkit_logout's new suspend and hibernation features.

**Nov 12, 2023**

jwmkit_logout now supports **suspend** and **hibernate**.


This entry has been edited. Configuration is now possible using the jwmkit_first_run tool.

INFO  

- The user defined method of requesting permission for jwmkit_logout will be used with these commands.

- The buttons for suspend and hibernate are only displayed if the values are set in the setting_su file.  
- You can choose suspend or hibernate, or both,  but the interface may look weird with an odd number off buttons.

**Nov 11, 2023**

jwmkit_keys now supports Task# (similar to Desktop#)  

Switch to a specific task in the tasklist with a mod key and a number key. For example with SHIFT as the mod key you could use SHIFT+2 to select the 2nd task in the tasklist

NOTE: You can not use the same mod key set for Desktop#

**Nov 10, 2023** - Second entry  

jwmkit_desktops now supports the backandforth option.  

Backandforth allows you to use the same keyboard shortcut to toggle between the current and previous virtual desktop.

*Example*  
  NOTE for this example  ALT+1 is configured  to switch to desktop 1.  
  
If you are on desktop 2 and press ALT+1 you will  switch to desktop 1.  
Hit the ALT+1 combo again and you will return to desktop 2.  
The pattern continues with each added press of ALT + 1 toggling between desktops 1 and 2.

**Nov 10, 2023**

jwmkit_appearance now supports these new jwm 2.4.3 options

- showkill - Determines if the Kill menu item is shown on task bars and window title bar
- showclient - Include the X client machine name in the window title

**Nov 06, 2023**

- Bugfix - Battery menu script would fail on some distros.

**Nov 01, 2023**

- Bugfix -  Fixed an issue on Debian 12 based distros where the list of timezone was incomplete in jwmkit_time .
- Traybutton, and battery icon improved for visibility. Greater contrast in colors.

**Oct 25, 2023**

- jwmkit_groups provides the new option nomaxtitle. A window in a group with this option set will not display the titlebar when maximized.
- A small fix for those using the jwmkit audio controls with sndio on a Debian 12 based distro. This should fix the problem with the sndio mixer not popping up when clicked,  
- [Click here](https://codeberg.org/JWMKit/JWM_Kit/wiki/smix)  for a note on how to implement the sndio mixer fix.

**Sept 23, 2022**

Wiki now has a page for JWM resources.  This page provides links to online resources with JWM / JWMKit related resources such as Themes or button sets.

**Aug 27, 2022**   Notice concerning the recent release.  

I failed to update the release version/date in the man pages prior to release.  
This has no ill effect on the software, but be aware that the version/date in the manpages will be 20220329 instead of the proper 20220826. I advise using the package manager to check your installed version.

**Aug 26, 2022**   New Stable Release  

This release includes the bugfix for issue #8.  
New packages and build scripts are avaliable [here](https://sourceforge.net/projects/jwmkit/files/) 


**Aug 25, 2022**  

Bugfix for issue #8.  Poorly made freedesktop entries that lack the opening \[Desktop Entry\] should no longer affect JWMKit.  

Apologies for taking so long to address this issue, but it could not be helped.    

 This is an important update for affected users.  So a new stable release is appropriate.  Hopeful I can find time to manage that before the weekend.  Including new packages for Arch, Hyperbola, Debian, and Puppy.

**March 29, 2022** New Stable Release (late entry)

- Bug fixes & improvements
- Enhancements for antiX user

**February 27, 2022**  
Since last release

- Fix for Include executables not loading on start up if the resolution is change by a JWM StartupCommand.
- antiX specific improvements for jwmkit_easymenu and jwmkit_settings
- JWM Kit Desktops 
- - Support changing zzzFM desktop's wallpaper
-  - Improve XML output

More Themes. 61 themes added.  

- 60 of the themes are from the puppy community. Thanks to all the creative people involved.  
 I have made changes to many of these to improve formatting, syntax, and compatibility with JWM Kit and newer version of JWM.
- JWMKit-DarkGreen-V2 is an updated version of the original JWMKit-Dark-Green theme.

| Link  | Description |
|--|--|
| [wiki theme page](https://codeberg.org/JWMKit/JWM_Kit/wiki/themes)  | Information and preview images. | 
| [All Themes zip archive](https://sourceforge.net/projects/jwmkit/files/themes/all_themes.zip/download)  | Download all themes (updated) |
| [All Newly Added zip archive](https://sourceforge.net/projects/jwmkit/files/themes/all_themes_update.zip/download)   | Download the newly added themes|

**February 14, 2022**

New Stable Release

JWM Kit Wallpaper replaced with the new JWM Kit Desktops

Most notable new features

- Specific different background for each Virtual Desktops
- configure the number of desktops
- configure additional window behaviors
- configure the button order for the window title bar
- NOTE : The new button order feature requires JWM 2.4 or greater

Important Release Info  
Any Reference to the old JWM Kit Wallpaper in your menu's and trays should be replaces with the new JWM Kit Desktop.  If you are using the menus provided with JWM Kit's default config this will include the menus : menu-jwmkit, and menu-right

**February 13, 2022**  

NOTICE : The JWM Kit Desktops' button order option requires JWM 2.4 or higher. This option is ignored when using older version of JWM.

**February 12, 2022**   - Second Entry

Updated wiki  
[JWM Kit Desktops wiki page](https://codeberg.org/JWMKit/JWM_Kit/wiki/desktop) 

**February 12, 2022**

Introducing **JWM Kit Desktops**  
Purpose &nbsp;: Configure Desktops and windows preferences.  
Replaces : JWM Kit Wallpaper  

**JWM Kit Desktops Features**  

Desktop background (image or color)  

- Set the default background 
- Set a different background for each desktop (virtual desktop)  
- The default background will be used when a desktop’s background is not defined
- Apply all option : sets all desktop to the same background  
This option set only the default and clears the rest
- Remove any specific background  
Includes the default  and/or any specific desktop’s backgrounds
- Option to remove all backgrounds
- Additional info on removing backgrounds.
- - Removing a background means the value is not defined.
- - If a specific desktop is removed it will use the default background, but If the default is also not defined it will use the background of the last selected desktop.
- - If all backgrounds are removed the background last uses will stay until the user logs off. When the user logs back in the display manager's background will be used.

Preferences. Configure the following

- Number of Virtual desktops in the vertical and horizontal direction
- Double click behavior
- Windows behaviors : Focus, Move, Resize, Snapping
- Configure the order of the buttons in the title bar of the windows

**February 03 2022**  

New Stable Release 20220203  
Fixes a few issues with yesterday's release.

- Fix incompatibility with older version of python. Tested on 3.5 
- Corrupt themes files no longer prevent JWM Kit Appearances from starting

**February 02 2022**

- New Stable Release 20220202  
- Create and edit Themes with JWM Kit Appearance  
More info on the JWM Kit Appearance [Wiki page](https://codeberg.org/JWMKit/JWM_Kit/wiki/appearances)

**January 21 2022**

In General

- Code clean up
- Improved comments
- Remove developer notes (comments, TODO, etc) that were no longer needed
- Convert use of some class/functions to equivalent ones that have proven to be more optimal
- Update man pages and other documentation
- Resumed work on the previous suspended [JWM Kit Designer](https://codeberg.org/JWMKit/JWM_Kit/wiki/design).  A tool for creating and editing JWM Themes

Repair &amp; Restore

- Automatically create the JWM Kit Settings file on startup if it is missing
- Automatically open the repair_settings dialog when creating the settings file
- BugFix : installing a restore point could fail when converting the default config location

**January 18 2022**

- Built in viewer for text files and archive contents
- Dialog to configure externals tools for JWM Kit's use  
Example :  text editor, archiver and file manager
- The features improve the out of box experience and should be well worth the slight addition of code size.

**January 17 2022**  - Second Entry

JWM Kit Repair &amp; Restore improvements 

- Resolve compatibility issues when installing a restore point created for different versions of JWM. 
- Restore points can be shared between machines using different versions of JWM
- Updated WIki to include info on new JWM Kit Keys.

**January 17 2022**  

Some INFO and clarification

- JWM 2.4 supported but NOT required by JWM Kit.  
 JWM Kit still supports older version of JWM as well as the new version 2.4.  You can use either.
- As some have speculated a forth release is coming soon.  If you are packaging JWM Kit for redistributing you can save yourself some time by wait for the new upcoming release.

**January 16 2022**  

Complete re-write of JWM Kit Keys

- 40% less code
- Lighter on resources
- Major interface improvements
- Easier to use
- Support for key, keycode, and mouse bindings

**January 13 2022**  
New Stable Release 20220113


**January 12 2022**  

Better late than never so "Happy 2022. I hope everyone had a very Merry Christmas."

- JWM Kit now support both HOME/.jwmrc and HOME/.config/jwm/jwmrc  
This is in support of the new JWM 2.4. 
- If both files are on your system and JWM is version 2.4 or above, JWM Kit will used the new .config/jwm/jwmrc over the old one. This is to be consistent with the behavior observed when testing JWM 2.4
- Reduction of total code size.

**October 24 2021**  

Many people have asked about JWM themes and buttons sets to use with JWM and JWM KIt.  I have add a collection to sourceforge. The collection includes official JWM Kit themes and button sets as well as many themes and button sets from the Puppy Linux community. 

You can preview and download individual themes or button set from either the JWM Kit *wiki* or *sourceforge*. An option is provided to download all themes or all buttons sets as a .zip archive.

- [JWM Kit Wiki](https://codeberg.org/JWMKit/JWM_Kit/wiki#support-files)   - Look in the "Support Files" section
- [JWM Kit's sourceforge](https://sourceforge.net/projects/jwmkit/files/) - Look in the "themes" and " theme_button_sets" directories  
- Download All Themes :  [all_themes.zip](https://sourceforge.net/projects/jwmkit/files/themes/all_themes.zip/download)
- Download All Button Sets :  [all\_buttons.zip](https://sourceforge.net/projects/jwmkit/files/theme_button_sets/all_buttons.zip/download) 

**October 05 2021**

- Expanded Pop Volume's sndio support
- Provide a enhanced mixer for sndio
- sndio users can now control the level of each stream providing individual volume control for media player, web browser, etc. 
- sndio users with alsa-utils installed have the ability to control both the alsa and sindo master.

**October 2 2021** 

- JWM Kit Icons now shows a preview icon when selecting a search result.

**October 01 2021** - Second Entry

- New option for JWM Kit Logout : doas with no password
- The [JWM Kit Forum](https://sourceforge.net/p/jwmkit/discussion/) is open. Join the discussion!  
Discussion in other public forums will continue, but this forum will provide more freedom for discussion of JWM Kit

**October 01 2021**  

- JWM Kit Pop Volume now support sndio
- - sndioctl must be present
- - No configuration or parameters required to select sndio
- - Sndio is used when ALSA's amixer is not available.
- Improvements for ALSA support 
- Optional Dependencies
- - alsa-utils,  sndio 

**September 29 2021**  

- JWM Kit now has a [Wiki](https://codeberg.org/JWMKit/JWM_Kit/wiki). The wiki provides basic information on JWM Kit's various tools with plenty of pictures.

**September 24 2021**  

- Initial Stable Release is finally stable No more changes.
- Again moved release tag to latest commit. Sorry but it was necessary
- Fixed Debian's changelog and packaging scripts in BUILD
- Moved release tag to latest commit to include the above fixes in the initial stable release

**September 23 2021**  

- Finally!, A stable release :  [Initial Stable Release](https://codeberg.org/JWMKit/JWM_Kit/releases) 
- New packages and build scripts will be available on sourceforge soon.


**September 8 2021**  

- Add permission button to jwmkit_first_run which provides the ablity to configure how JWM Kit request permission.  
- Added the configuration file ~/.config/jwmkit/setting_su to record's the users preferences on how permission is requested.  
- Added a built in graphical password prompt to be used with sudo  

**September 8 2021**

- install packages for various distos moved to [Sourceforge](https://sourceforge.net/projects/jwmkit/files/Packages/)   
- A cleanup/restructuring of this Git (codeberg) is underway.  Sorry if this breaks any hyperlinks, bookmarks, scripts, etc  
- Preview images moved to sourceforge and google drive to reduce the tarball size of tagged releases
- Apologies to [vitforlinux](https://vitforlinux.wordpress.com/) as my recent changes will break the links in your article.  Thanks for the review.

---
### Dependencies

NOTICE : The listed dependencies use Debain package names. These names on maybe different non-Debian based distro.

Required

* python3
* python3-gi
* gir1.2-gtk-3.0  

optional (for pop up volume control / notification)

* alsa-utils
* sndio

---
### DOWNLOADS

| Download | Description |
|--|--|
|  [DEB ](https://sourceforge.net/projects/jwmkit/files/Packages/Debian/)   | packages for Debian, Devuan, Ubuntu, etc |
|  [Arch ](https://sourceforge.net/projects/jwmkit/files/Packages/Arch/) | Packages for Arch, Manjaro, etc|
| [Puppy](https://sourceforge.net/projects/jwmkit/files/Packages/Puppy/)  | Packages for FOSSA & SLACKO Puppy Linux |
| [Build Script](https://sourceforge.net/projects/jwmkit/files/BUILD/) | make your own package |
|  [JWM Kit Linux](https://sourceforge.net/projects/jwmkit/files/iso/)  | Evaluate  JWM Kit in a live boot enviroment |

---
### Some of JWM Kit's Features

* No part of this software is designed to runs in the background. It only runs when the user starts it, and properly quits when "closed", or for notifications will close when timed out in a few seconds.
* Features are implemented using JWM's existing abilities.
* A settings manager as seen in popular desktops
* Set the desktop background as an image, solid color or gradient.
* configure icon paths
* Preview and Switch JWM Themes
* Preview and Switch JWM button sets (window buttons like: close, max, min, etc)
* Create a new button set by replacing the colors on an existing set (SVG)
* Configure the appearance and behavior of JWM trays, and menus.
* Add & Remove JWM trays, and menus
* Add & Remove items to trays, and menus
* Configure commands (scripts, apps, etc) to run when JWM starts, restarts and/or shutdown
* Save current Screen configuration for startup (resolution/frame rate/monitor position/etc)
* Configure key board shortcuts for custom controls, and/or quick access
* add a simple popup calendar to the tray to the clock (or icon/key shortcut)
* add a battery monitor to a tray icon or keyboard shortcuts
* add volume notification and control to an tray icon or keyboard shortcuts
* Specify options for a group of programs by name or class
* Create a restore point of the JWM configuration
* Restore the JWM configuration to an earlier time/date
* Receive feedback useful for finding and repairing the JWM Configuration

As they say a picture is worth a thousand words. Previews are avalible at the following locations:

- [Google Drive](https://drive.google.com/drive/folders/1j4nRrMzOq_kXwCBDyKIgVFnuKRZYRg_U?usp=sharing) 
- [Sourceforge](https://sourceforge.net/projects/jwmkit/files/preview_images/ )  
- The [Wiki](https://codeberg.org/JWMKit/JWM_Kit/wiki) also contains a good number of pictures  

---
### Summary of JWM Kit's tools  
You can Read more about these tools in the [wiki](https://codeberg.org/JWMKit/JWM_Kit/wiki#support-files) 

| Tool | Purpose |
|--|--|
|[First Run](https://codeberg.org/JWMKit/JWM_Kit/wiki/firstrun) |Assistance after the initial install of JWM Kit|
|[Appearance](https://codeberg.org/JWMKit/JWM_Kit/wiki/appearances) |Preview and set JWM Theme, and/or button set|
|[Menus](https://codeberg.org/JWMKit/JWM_Kit/wiki/menus) |Configure the appearance and behavior of JWM menus
|[Trays](https://codeberg.org/JWMKit/JWM_Kit/wiki/trays) |Configure the appearance and behavior of JWM trays|
|[Repair & Restore](https://codeberg.org/JWMKit/JWM_Kit/wiki/repair) |Tools to repair the JWM configuration|
|[Settings](https://codeberg.org/JWMKit/JWM_Kit/wiki/settings) |A settings manager as seen in popular desktops|
|[Freedesktop](https://codeberg.org/JWMKit/JWM_Kit/wiki/freedesktop) | Freedesktop entries  (Similar to Menulibe)|
|Easy Menu|Generate an Application Menu from freedesktop files|
|[Calendar](https://codeberg.org/JWMKit/JWM_Kit/wiki/cal) |pop up calendar for tray|
|[Groups](https://codeberg.org/JWMKit/JWM_Kit/wiki/groups) |Specify options for a group of programs by their name and/or class|
|[Icon](https://codeberg.org/JWMKit/JWM_Kit/wiki/icons) |Tell JWM where to find icons|
|[Keys](https://codeberg.org/JWMKit/JWM_Kit/wiki/keys) |Configure mouse and keyboard shortcuts|
|[Logout](https://codeberg.org/JWMKit/JWM_Kit/wiki/logout) |Simple Dialog with Logout, reboot, and shutdown options|
|Pop Volume|Assign volume control and notification to a tray icon or key shortcuts|
|[Startups](https://codeberg.org/JWMKit/JWM_Kit/wiki/startups) |Configure commands (scripts, apps, etc) to run when JWM starts, restarts and/or shutdown|
|[Desktops](https://codeberg.org/JWMKit/JWM_Kit/wiki/desktop) |Set desktop backgrounds  &amp; configure window/desktop behavior|
|[Time](https://codeberg.org/JWMKit/JWM_Kit/wiki/time) |Set the System's time, date, and time zone|
|Battery Menu|Generate a JWM Menu that provides battery info|

---
#### Behavior that may or may not be obvious   
* Entries in JWM Kit Startups tool must have at least one option selected. Options are Startup, Restart, Shutdown. Items without an option selected will not be saved.  
* Reminder of the obvious: Although you may see your changes with in an Application, Changes are not written to disk until you press the save button.
* Comments are not preserved in the XML files.  While its not been well tested, JWM Kit seems to have no problem processing XML files with comments.   
* JWM Kit uses the $HOME variable to represent the users home directory. This allowing a config files to be used by another user.  

---
#### Validation check  
JWM Kit does check the config files for proper form and syntax.  Sometimes it may check a file for it's intended purpose. While these checks are helpful, they are limited.  JWM Kit maybe unable to use a file If it was improperly edit either manually or by another program.
